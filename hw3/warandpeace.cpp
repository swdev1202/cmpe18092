#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>

using namespace std;

// Names to search for.
const string MAKAR  = "Makar Alexeevich";
const string JOSEPH = "Joseph Bazdeev";
const string BORIS  = "Boris Drubetskoy";

const string INPUT_FILE_NAME = "WarAndPeace.txt";

void printHeader();
void printLocation(int lineNum, int idx, string name);

void firstAndLastNames(const vector<string> &a, vector<vector<string>> &out);

int main()
{
    ifstream input;
    input.open(INPUT_FILE_NAME);
    if (input.fail())
    {
        cout << "Failed to open " << INPUT_FILE_NAME << endl;
        return -1;
    }

    printHeader();

    string readLine = "";
    int lineNumber = 0;

    vector<string> names = {MAKAR, JOSEPH, BORIS};
    vector<vector<string>> firstlastNames;
    firstAndLastNames(names, firstlastNames);

    int firstNameFoundPosition = 0;
    int firstNameFoundLinenumber = 0;
    vector<bool> firstNameFound = {false, false, false};

    while(getline(input, readLine))
    {
        lineNumber++;
        
        // first, only look for the first name
        for(int i=0; i < names.size(); i++)
        {
            string firstName = firstlastNames.at(i).at(0); // first name
            int position = readLine.find(firstName);
            
            if(position != -1) //if found the first name
            {
                //see if the full name is available
                if(readLine.find(names.at(i)) != -1)
                {
                    // if able to find the full name, print out to the console
                    printLocation(lineNumber, position, names.at(i));
                }
                else
                {
                    // if not able to find the full name, we can assume it might have the
                    // last name at the next line, so raise the flag
                    firstNameFound.at(i) = true;
                    firstNameFoundPosition = position;
                    firstNameFoundLinenumber = lineNumber;
                }
            }

            // at the very next line where first name was discovered,
            // look for which first name was discovered
            if((lineNumber == (firstNameFoundLinenumber+1)) && firstNameFound.at(i)) // if this current line number matches
            {
                // get a last name
                string lastName = firstlastNames.at(i).at(1);
                // if the last name's position is at the beginning of the line
                if(readLine.find(lastName) == 0)
                {   //print the result
                    printLocation(firstNameFoundLinenumber, firstNameFoundPosition, names.at(i));
                }
                else
                {
                    firstNameFound.at(i) = false;
                    firstNameFoundPosition = 0;
                    firstNameFoundLinenumber = 0;
                }
            }
        }
    }

    return 0;
}

void printHeader()
{
    cout << setw(5) << "LINE";
    cout << setw(10) << "POSITION";
    cout << setw(6) << "NAME" << endl;
}

void printLocation(int lineNum, int idx, string name)
{
    cout << setw(5) << lineNum;
    cout << setw(10) << (idx+1); // idx+1 is necessary becuase 1st element is the 0.
    cout << "  " << name;
    cout << endl;
}

void firstAndLastNames(const vector<string> &a, vector<vector<string>> &out)
{
    for(int i=0; i < a.size(); i++)
    {
        string fullName = a[i];
        string storeWord = "";
        vector<string> in_temp;
        for(int j=0; j < fullName.length(); j++)
        {
            char temp = fullName[j]; // get each character
            if(temp == ' ') // if a space separated
            {
                in_temp.push_back(storeWord);
                storeWord = "";
            }
            else
            {
                storeWord += temp;
            }
        }
        //put the last piece into the vector
        in_temp.push_back(storeWord);
        out.push_back(in_temp);
    }
}