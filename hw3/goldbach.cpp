/*
 * Author: Seung Won Lee
 * SJSU FALL 2016 CMPE180-92
 */

#include <iostream>
#include <iomanip>
using namespace std;

/*
 * Function: initArray
 * Init array fills the given array of n elements.
 * Each array element holds from 1 to n.
 * In other words, a[i] = i+1;
 * @param a[] - un-initialized int array
 * @param size - the size of the given array
 */
void initArray(int unsortedArray[], int size);

/*
 * Function: primeSort
 * primeSort function marks non-prime number
 * with 0 while leaving prime number as it is.
 * @param unsortedArray[] - initialized array with integers
 * @param size - the size of the unsortedArray
 */
void primeSort(int unsortedArray[], int size);

/*
 * Function: printPrime
 * This function takes already sorted prime array
 * and prints 10 prime numbers each row.
 * If it's not a prime number, it replaces with "."
 * @param primeSortedArray[] - takes the array with prime numbers
 * @param size - the size of the initial array
 */
void printPrime(int primeSortedArray[], int size);

 /*
  * Function: printCount
  * This function takes the the prime sorted array and
  * counts the number of primes
  * @param primeSortedArray[] -  original array with prime sorted
  * @param size - the size of the initial array
  * return the number of prime numbers 
  */
int primeCount(int primeSortedArray[], int size);

/*
 * Function: storePrime
 * It takes the prime sorted array and store each prime number
 * into a separate "Prime-only" array
 * @param primeSortedArray - original array with prime sorted
 * @param size - the size of the original array,
 * @param primeArray - this array stores only the prime numbers
 */
void storePrime(const int primeSortedArray[], const int size ,int primeArray[]);

/*
 * Function: printGoldbach
 * It takes the original size of the array and the "prime-only" array
 * to calculate the goldbach's conjecture.
 * @param testingSize - the size of the original array
 * @param primeArray - "prime-only" array
 * @param primeArraySize - size of the "prime-only" array 
 */
void printGoldbach(int testingSize,const int primeArray[], const int primeArraySize);


const int ARRAYSIZE = 100;

int main()
{
	// first, create an array with a given array size.
	int originalArray[ARRAYSIZE];

	// this will fill up elements with integers
	initArray(originalArray, ARRAYSIZE);

	// it sorts out only the prime numbers in given size
	primeSort(originalArray, ARRAYSIZE);

	// count number of prime numbers in the initial array	
	int numberOfPrime = primeCount(originalArray, ARRAYSIZE);

	// create a new array with the size equal to the number of prime numbers
	int primeOnlyArray[numberOfPrime];

	// store only prime numbers in a prime-only array
	storePrime(originalArray, ARRAYSIZE, primeOnlyArray);

	// print the prime numbers. If not a prime number, print "."
	printPrime(originalArray, ARRAYSIZE);

	// print the goldbach's conjecture results
	printGoldbach(ARRAYSIZE, primeOnlyArray, numberOfPrime);
}

void initArray(int a[], int size)
{
	for (int i = 0; i < size; i++)
	{
		a[i] = i+1;
	}
}

void primeSort(int unsortedArray[], int size)
{
	for(int i = 0; (i*i) < size; i++)
	{
		if(i == 0)
		{
			// we know the first number in the array is 1 and
			// 1 is not the prime.
			// therefore, we are initially replacing it with 0.
			unsortedArray[i] = 0; // replace with 0
		}
		else
		{
			// if accessed element is not replaced with 0, it's a candidate for a prime number
			if(unsortedArray[i] != 0)
			{
				int primeNum = unsortedArray[i];
				// given a prime number, mark all multiples of the prime number with 0s.
				// [e.g. if primeNum = 2, replaced all multiples of 2 (4,6,8,10,...) with 0]
				for(int j = ((primeNum*primeNum)-1); j <= size; j+=primeNum)
				{
					unsortedArray[j] = 0;
				}
			}
		}
	}
}

void printPrime(int primeSortedArray[], int size)
{
	cout << "Primes:" << endl << endl;

	// for the entire array,
	for(int i = 0; i < size; i++)
	{
		if(primeSortedArray[i] == 0)
		{
			// if it's makred as non-prime number, print '.'.
			cout << setw(3) << ".";
		}
		else
		{
			// if it's a prime number, print it.
			cout << setw(3) << primeSortedArray[i];
		}

		if(((i+1)%10) == 0)
		{
			// line separation every 10 numbers
			cout << endl;
		}
	}
	cout << endl;
}

int primeCount(int primeSortedArray[], int size)
{
	int cnt = 0;
	for(int i = 0; i < size; i++)
	{
		// if accessed element is not 0 (in other words, a prime number)
		// count increment
		if(primeSortedArray[i] != 0)
		{
			cnt++;
		}
	}
	return cnt;
}

void storePrime(const int primeSortedArray[], const int size ,int primeArray[])
{
	int foundPrime = 0;
	for(int i = 0; i < size; i++)
	{
		if(primeSortedArray[i] != 0)
		{
			//if it's a prime number
			primeArray[foundPrime] = primeSortedArray[i];
			foundPrime++;
		}
	}
}


void printGoldbach(int testingSize,const int primeArray[], const int primeArraySize)
{
 	cout << "Test of Goldbach's Conjecture:" << endl << endl;

 	// test all even integers starting from 4 to n (the size of the initial array)
 	for(int i=4; i<=testingSize; i+=2)
 	{
 		cout << setw(3) << i << ":";
 		
 		// just a flag to indicate if it's a first result to print for a given even number
 		bool firstDisplay = true;

 		// given a prime-only array, start from the first prime number
 		// until the last prime number in an array
 		for(int j=0; j < primeArraySize; j++)
 		{
 			// first prime number to test goldbach's conjecture
 			int getFirstNum = primeArray[j];

 			// this skips us to count the repeating conjecture
 			// [e.g. if even number is 10, the possible results are
 			// "3+7" , "5+5" , "7+3"
 			// However, 3+7 and 7+3 are the same. Therefore, first number must not exceed
 			// the half of the given even number.
 			if(getFirstNum > (i/2))
 			{
 				break;
 			}
 			else
 			{
 				// select the second prime number from the first element to the last
	 			for(int k=0; k < primeArraySize; k++)
	 			{
	 				// second prime number to test goldbach's conjecture
	 				int getSecondNum = primeArray[k];
	 				// if the sum of those two prime numbers is equal to the given even integer
	 				if((getFirstNum + getSecondNum) == i)
	 				{
	 					// when first result to display,
	 					if(firstDisplay)
	 					{
	 						cout << setw(3) << getFirstNum << setw(3) << getSecondNum << endl;
	 						// flag it to false since we already had the first result
	 						firstDisplay = false;
	 					}
	 					else
	 					{
	 						cout << setw(7) << getFirstNum << setw(3) << getSecondNum << endl;
	 					}	 				}
	 			}
	 		}
 		}
 		// print the new line unless it's at the end of the results
 		// because it will not match the expected output.
 		if(i != testingSize)
 		{
 			cout << endl;
 		}
 	}
}