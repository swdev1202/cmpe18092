
#include "hash.h"

int hashCode(int x, int n)
{
   if (x >= 0) return x % n; else return -x % n;
}

/**
   Implement the algorithm for inserting an element in a hash table
   with linear probing. The probing sequence simply visits

   h, h + 1, h + 2, ...,

   reduced modulo the table size n.

   If the element is present or the table is full, do not modify the table.

   For simplicity, you store integers in the table. Use the given
   hashCode function for hashing them.
*/

void insert(vector<int>& table, int element)
{
   int h = hashCode(element, table.size()); // hased location

   int elementFound = 0;
   for(int i = 0; i < table.size(); i++){
       if(table.at(i) == element){
           elementFound = 1;
       }
   }

   if(!elementFound){ // if element was not found in the table
       // insert it
       if(table.at(h) == EMPTY){
           // if this hased location is empty, insert the element
           table.at(h) = element;
       }
       else{
           // if this hased location is occupied by another element,
           int idx = h;
           int hashDone = 0;
           for(;idx < table.size(); idx++){
               // from the location h, start looking at the possible empty position
               if(table.at(idx) == EMPTY){
                   table.at(idx) = element;
                   hashDone = 1;
                   break;
               }
           }
           if(!hashDone){
               // if it's stil not done,
               for(idx = 0; idx < h; idx++){
                   //start from the beginning
                   if(table.at(idx) == EMPTY){
                       table.at(idx) = element;
                       break;
                   }
               }
           }
       }

   }
}
