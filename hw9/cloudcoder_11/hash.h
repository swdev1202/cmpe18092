#include <vector>

using namespace std;

const int EMPTY = 0x80000000;
int hashCode(int x, int n);
int find(const vector<int>& table, int element);
