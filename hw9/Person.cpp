#include <iostream>
#include <string>
#include <vector>
#include <iomanip>
#include "Person.h"
using namespace std;

/**
 * Constructors
 */
Person::Person(int level, string name)
{
    this->level = level;
    this->name = name;
    this->spouse_name = "";
    this->parent = nullptr;
}

Person::Person(int level, string name, string spouse)
{
    this->level = level;
    this->name = name;
    this->spouse_name = spouse;
    this->parent = nullptr;
}

/**
 * Destructor
 */
Person::~Person()
{
    if(!this->children.empty())
    {
        vector<Person*>::iterator it = this->children.begin();
        while(it != this->children.end())
        {
            it = this->children.erase(it);
        }
    }
}

/**
 * Have a child.
 * @param child pointer to the new child.
 */
void Person::have_child(Person *child)
{
    children.push_back(child); // store the argument child into the children vector.
    child->parent = this; // make this child's parent to the callee.
}

/**
 * Print a person.
 */
void Person::print() const
{
    if(this->parent == nullptr) // first generation
    {
        if(this->spouse_name != "") {cout << this->name << " (" << this->spouse_name << ")" << endl;}
        else {cout << this->name << endl;}
        cout << "|" << endl;
    }
    else // at least a second generation
    {
        if(this->level >= 2)
        {
            if(this->parent->name == (this->parent->parent->children.back()->name))
            {
                // no bar
                cout << setw(4 * (this->level)) << "+---";
            }
            else
            {
                cout << setw(5 * (this->level-2));
                cout << "|"; 
                cout << "   " << "+---";
            }
        }
        else
        {
            cout << setw(4 * (this->level)) << "+---";
        }


        if(this->spouse_name != "")
        {   
            cout << this->name; // print the name of a person
            cout << " (" << this->spouse_name << ")" << endl; // and print the name of the spouse
        }
        else
        {
            cout << this->name << endl;
        }

        print_bar();
    }


    if(!this->children.empty()) //if not empty
    {
    	vector<Person*>::const_iterator it = children.begin();
    	for(; it != this->children.end(); it++)
    	{
    		//Person *temp = *it;
    		(*it)->print();
    	}
    }
}

/**
 * Print the vertical bar.
 */
void Person::print_bar() const
{
    int parentLevel = this->parent->level;
    int currentLevel = this->level;

    if(this->name != (this->parent->children.back()->name)) // if this person is not the last child
    {
        if(!this->children.empty()) // and this person has children
        {
            cout << setw(5*(currentLevel-1)) << "|";
            cout << "   |" << endl;
        }
        else // has no children
        {
            if(currentLevel >= 2)
            {
                if(this->parent->name == (this->parent->parent->children.back()->name))
                {
                    if(this->children.empty())
                    {
                        cout << setw(4*(currentLevel-1)+1) << "|" << endl;
                    }
                }
                else
                {
                    cout << setw(5*(currentLevel-2)) << "|";
                    cout << "   |" << endl;
                }
            }
            else
            {
                cout << "|" << endl;
            }
        }
    }
    else
    {
        if(currentLevel >= 2)
        {
            if(!this->children.empty())
            {
                cout << setw((5 * (currentLevel)) - 1) << "|" << endl;
            }
            else
            {
                if(this->parent->name != this->parent->parent->children.back()->name)
                {
                    cout << setw(5*(currentLevel-2)) << "|" << endl;
                }
            }
        }
        else
        {
            cout << setw(5) << "|" << endl;
        }
    }
}