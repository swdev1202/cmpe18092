#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

using namespace std;

const string INPUT_FILE_NAME = "students.txt";

/*
 * Function: getNumberofStudents
 * This function will take the first line of the .txt file and obtain
 * the course name and the number of students
 * @param readLine - a string line read from the input text file
 * @param courseName - passing a string parameter by reference to store the course name
 * return an integer value of number of students
 */
int getNumberofStudents(string readLine, string& courseName);

/*
 * Function: buildDArrays
 * This function will create various dynamic arrays (1D and 2D)
 * @param readLine - a string line read from the input text file
 * @param name - a string pointer to the string arrays that have students' names
 * @param score - an int pointer to pointer to the 2D arrays that have students' scores
 * @param column - an int pointer to the array that have the number of scores each student has
 * @param lineNum - the line number tells in which line it is reading in the txt file
 */
void buildDArrays(string readLine, string *name, int **score, int *column, int lineNum);

/*
 * Function: printDArrays
 * This function simply prints out using the arrays that we obtained fro buildDArrays
 * @param name - a string pointer to the string arrays that have students' names
 * @param score - an int pointer to pointer to the 2D arrays that have students' scores
 * @param column - an int pointer to the array that have the number of scores each student has
 * @param studentNumber - number of total students
 * @param courseName - the name of the course
 */
void printDArrays(string *name, int **score, int *column, int studentNumber, string courseName);

/*
 * Function: deleteDArrays
 * This function deallocates (free) the allocated memories from the "new []" operations
 * @param name - a string pointer to the string arrays that have students' names
 * @param score - an int pointer to pointer to the 2D arrays that have students' scores
 * @param column - an int pointer to the array that have the number of scores each student has
 * @param studentNumber - number of total students
 */
void deleteDArrays(string *name, int **score, int *column, int studentNumber);

/**
 * Main.
 */
int main()
{
    ifstream input;
    input.open(INPUT_FILE_NAME);
    if (input.fail())
    {
        cout << "Failed to open " << INPUT_FILE_NAME << endl;
        return -1;
    }

    string line;
    int lineNumber = 0;
    int numStudent = 0;
    string nameoftheCourse = "";

    string *names = nullptr;
    int **scores = nullptr;
    int *columnSize = nullptr;

    while(getline(input, line))
    {
    	lineNumber++;

    	if(lineNumber == 1) // the first line
    	{
    		numStudent = getNumberofStudents(line, nameoftheCourse);
    		names = new string[numStudent];
    		scores = new int*[numStudent];
    		columnSize = new int[numStudent];
    	}
    	else
    	{
	    	buildDArrays(line, names, scores, columnSize, lineNumber);
	    }
    }

    printDArrays(names, scores, columnSize, numStudent, nameoftheCourse);
    deleteDArrays(names, scores, columnSize, numStudent);
}

int getNumberofStudents(string readLine, string& courseName)
{
	string temp = "";
	for(int i=0; i<readLine.length(); i++)
	{
		if(readLine[i] == ' ')
		{
			courseName = temp;
			temp = "";
		}
		else
		{
			temp += readLine[i];
		}
	}
	return stoi(temp);
}

void buildDArrays(string readLine, string *name, int **score, int *column, int lineNum)
{
	string temporalString = "";
	int spaceCnt = 0; // This keeps track of how many spaces there were when traversing a given string
	int numScores = 0; // It holds how many scores are availalbe for ecah student
	int rowIdx = lineNum - 2; // Student's name and score appear from line number 2.
							  // This is used as a row index.

	for(int i=0; i < readLine.length(); i++)
	{
		if(readLine[i] == ' ') // every time it sees an empty space
		{
			spaceCnt++;

			if(spaceCnt == 2) // first and last name have been detected
			{
				name[rowIdx] = temporalString; // save the student's name at the given location
												 // [e.g.] if linuNum = 2, rowIdx = 2-2 = 0;
												 // therefore, it is the first student's first and last name
												 // and names[rowIdx] = names[0] = "First Student"
				temporalString = ""; // initialize the string to store other data
			}
			else if(spaceCnt == 3) // number of scores detected
			{
				numScores = stoi(temporalString); // convert the string-type integer to real integer
												  // and store it into the numScores variable

				score[rowIdx] = new int[numScores]; // create an array of integers to hold scores
				column[rowIdx] = numScores;
				temporalString = ""; // init the tem
			}
			else if(spaceCnt > 3) // start reading scores
			{
				int columnIdx = spaceCnt - 4;		   // spaceCnt - 4 will give the column index of the 2d array
				score[rowIdx][columnIdx] = stoi(temporalString); // store the score
				temporalString = "";
			}
			else // only at spaceCnt is 1
			{
				temporalString += readLine[i];
			}
		}
		else // if the character read is not a space, just concatenate into a temporary string
		{
			temporalString += readLine[i];
			if(i == readLine.length()-1)
			{
				if(stoi(temporalString) == 0) // if the very last string is 0 it's no score
				{
					score[rowIdx] = nullptr;
					column[rowIdx] = 0;
				}
				else
				{
					// if on the very last character,
					int columnIdx = spaceCnt - 3;
					score[rowIdx][columnIdx] = stoi(temporalString);
					temporalString = "";
				}
			}
		}
	}
}

void printDArrays(string *name, int **score, int *column, int studentNumber, string courseName)
{
	cout << "STUDENT SCORES for " << courseName << endl << endl;
	for(int i=0; i<studentNumber; i++)
	{
		cout << name[i] << endl; // print out the name of the student
		int columnSize = column[i]; // the number of scores for this students
		
		if(columnSize == 0) // if no score is available
		{
			cout << setw(4) << "(none)";
		}
		else
		{
			for(int j = 0; j < columnSize; j++)
			{
				cout << setw(4) << score[i][j];
			}
		}
		cout << endl << endl;
	}
}


void deleteDArrays(string *name, int **score, int *column, int studentNumber)
{
	delete [] name;
	delete [] column;
	for(int i=0; i<studentNumber; i++)
	{
		delete [] score[i];
	}
	delete [] score;
}