#include "BankAccount.h"

BankAccount::BankAccount()
{   
   balance = 0;
}

void BankAccount::deposit(double amount)
{  
    balance += amount;
}

// Declare the withdraw method
void BankAccount::withdraw(double amount)
{
    // if they charge a dollar per withraw...
    balance = balance - amount - 1;
}

double BankAccount::getBalance()
{   
   return balance;
}

