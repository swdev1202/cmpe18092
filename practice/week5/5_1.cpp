#include <cmath>

#include "point.h"

/**
   Compute the distance between two points.
   @param a, b two points
   @return the distance between them.
*/
double distance(Point a, Point b)
{
   double yDiff = b.y - a.y;
   double xDiff = b.x - a.x;
   
   yDiff *= yDiff;
   xDiff *= xDiff;
   
   return sqrt(yDiff + xDiff);
}

/**
   Compute the midpoint between two points.
   @param a, b two points
   @return the point halfway between them.
*/
Point midpoint(Point a, Point b)
{
   Point ret;
   ret.x = (a.x + b.x) / 2;
   ret.y = (a.y + b.y) / 2;
   return ret;
}
