
#include <cmath>

#include "point.h"

Point::Point(double xvalue, double yvalue)
{
   x = xvalue;
   y = yvalue;
}

double Point::get_x() { return x; }
double Point::get_y() { return y; }

/**
   Compute the distance between this point and another point.
   @param other the other point
   @return the distance between them.
*/
double Point::distance(Point other)
{
    double xDiff = other.get_x() - x;
    double yDiff = other.get_y() - y;
    
    yDiff *= yDiff;
    xDiff *= xDiff;
    
    return sqrt(yDiff + xDiff);
}

/**
   Compute the midpoint between this point and another point.
   @param other the other point
   @return the point halfway between them.
*/
Point Point::midpoint(Point other)
{
    double yMid = (other.get_y() + y)/2;
    double xMid = (other.get_x() + x)/2;
    
    Point ret(xMid, yMid);
    
    return ret;
   
}
