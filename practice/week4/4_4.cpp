
/**
   Given pointers start and end that point to the 
   first and past the last element of a segment inside
   an array, return a new array holding the reverse
   of the segment. 
*/
int* reverse(int* start, int* end)
{
    int gap = end - start;
    int *retArray = new int[gap];
    
    for(int i=0; i<gap; i++)
    {
        *(retArray+i) = *start;
        start++;
    }
    
    
    for(int i=0; i<(gap/2); i++)
    {
        int temp = *(retArray+i);
        *(retArray+i) = *(retArray+gap-1-i);
        *(retArray+gap-1-i) = temp;
    }
    
    return retArray;
}
