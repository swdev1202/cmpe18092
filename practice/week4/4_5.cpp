#include <cstring>
#include <iostream>

using namespace std;

/**
   Given an array of strings of length n > 0, return the longest
   string. If there are multiple strings of the same maximum length,
   return the first one.
*/
char* longest(char** words, int n)
{
    if(n == 1)
    {
        return *words;
    }
    else
    {
        string longest = *words;
        int record_i = 0;
        for(int i=1; i<n; i++)
        {
            string temp = *(words+i);
            if(temp.length() > longest.length())
            {
                longest = temp;
                record_i = i;
            }
        }
        return *(words+record_i);
    }
    
}
