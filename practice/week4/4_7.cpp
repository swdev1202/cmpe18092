#include <cstring>=

using namespace std;

/**
   Given a '\0'-terminated character array, split it by replacing 
   each space in the character array with a '\0' and return a 
   newly allocated array of char* pointers to the resulting strings.
*/
char** split(char* words)
{
   int count = 0;
   // count spaces
   int i = 0;
   while(1)
   {
       if(*(words+i) == ' ') // if found a space
       {
           count++;
           *(words+i) = '\0';
       }
       else if(*(words+i) == '\0')
       {
           count++;
           break;
       }
       i++;
   }
   
   // store results
   char** result = new char*[count];
   
   int t = 0;
   int cnt = 0;
   for(int i =0; i<count; i++)
   {
       while(1)
       {
           if(*(words+t) == '\0')
           {
               *(result+i) = new char[cnt];
               *(result+i) = (words+t-cnt);
               cnt = 0;
               t++;
               break;
           }
           else
           {
               t++;
               cnt++;
           }
       }
   }
   
   return result;
}
