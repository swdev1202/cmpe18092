/**
   Compute the minimum and maximum value in a non-empty array.
   @param arr the array
   @param n the length of the array
   @param min a pointer to a variable holding the minimum
   @param max a pointer to a variable holding the minimum
*/

void minmax(int* arr, int n, int* min, int* max)
{
    if(n == 1)
    {
        *min = *arr;
        *max = *arr;
    }
    else
    {
        *min = *arr;
        *min = *arr;
        arr++;
        
        for(int i=1; i<n; i++)
        {
            if(*arr < *min)
            {
                *min = *arr;
            }
            
            if(*arr > *max)
            {
                *max = *arr;
            }
            arr++;
        }
    }
}
