//SOLUTION
/**
   Return a pointer to the first negative element in 
   the given array which has length n. If there is no negative element,
   return a pointer past the last element. 
*/
int* firstneg(int* arr, int n)
{
    for(int i=0; i<n; i++)
    {
    	if(*arr < 0)
    	{
    		return arr;
    		break;
    	}
    	else
    	{
    		arr++;
    	}
    }
    return arr;
}
