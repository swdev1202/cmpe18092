#include <cstring>

using namespace std;

/**
   Given a '\0'-terminated character array, split it by replacing 
   each space in the character array with a '\0'. Return the number
   of strings into which you have split the input.
*/
int split(char* words)
{
   int stringCnt = 0;
   int idx = 0;
   while(1)
   {
       if(*(words+idx) == ' ')
       {
           *(words+idx) = '\0';
           stringCnt++;
           idx++;
       }
       else if(*(words+idx) == '\0')
       {
           stringCnt++;
           break;
       }
       else
       {
           idx++;
       }
   }
   return stringCnt;
}
