
/**
   Given pointers start and end that point to the 
   first and past the last element of a segment inside
   an array, reverse all elements in the segment.
*/

#include <iostream>
using namespace std;

void reverse(int* start, int* end)
{
    int gap = end - start;
    
    for(int i=0; i<(gap/2); i++)
    {
        int temp = *start;
        *start = *(end-1);
        *(end-1) = temp;
        start++;
        end--;
    }
}
