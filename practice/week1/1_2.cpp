#include <iostream>
using namespace std;

int main()
{
   cout << "Hours of first time: ";
   int hours1;
   cin >> hours1;
   cout << "Minutes of first time: ";
   int minutes1;
   cin >> minutes1;
   cout << "Hours of second time: ";
   int hours2;
   cin >> hours2;
   cout << "Minutes of second time: ";
   int minutes2;
   cin >> minutes2;
   cout << endl;
   
   // If the first time comes before the second time, print BEFORE
   // If the first time comes after the second time, print AFTER
   // If they are the same time, print SAME;
   
   // check the first and second hour
   if(hours1 < hours2)
   {
       //if first hour is before second hours
       cout << "BEFORE" << endl;
   }
   else if(hours1 == hours2)
   {
       //if two hours are the same, compare the minutes
       if(minutes1 < minutes2)
       {
           //if first minute is before the second minutes
           cout << "BEFORE" << endl;
       }
       else if(minutes1 == minutes2)
       {
           //if the minutes are the same
           cout << "SAME" << endl;
       }
       else
       {
           //the first minute is after the second minute
           cout << "AFTER" << endl;
       }
   }
   else
   {
       // if the first hour is after the second hour
       cout << "AFTER" << endl;
   }
   
   return 0;
}
