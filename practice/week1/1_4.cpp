#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
   cout << "Enter values, -1 when done: ";
   int result;
   // Read in numbers from cin. Stop when a value of -1 is entered.
   // Print the input that is closest to 100. If there are multiple
   // inputs that have the same minimal distance 100,
   // print the first one.
   // If no input was provided (other than the -1 sentinel),
   // print -1
   
   int input;
   int inputCnt = 0;
   while(1)
   {
       cin >> input;
       if(input == -1)
       {
           break;
       }
       else
       {
           int distance = abs(100-input);
           if(!inputCnt) // if this is the first input
           {
               result = input; // store the input as a result
               inputCnt++;
           }
           else
           {
               // compare the distance
               int resultDistance = abs(100-result);
               if(resultDistance > distance)
               {
                   result = input; // new input is the new result
               }
               else
               {
                   result = result; // otherwise, stays the same
               }
               inputCnt++;
           }
       }
   }
   if(!inputCnt) // if there were no input, print -1
   {
       result = -1;
   }
   
   cout << endl << "Result: " << result << endl;
   return 0;
}
