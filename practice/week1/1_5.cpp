#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
   cout << "Enter n: ";
   int n;
   cin >> n;
   cout << endl;
   
   // Draw the following "x in a box" pattern. There are n asterisks
   // on each side. In this example, n is 8.
   // ********
   // **    **
   // * *  * *
   // *  **  *
   // *  **  *
   // * *  * *
   // **    **
   // ********

   for(int y=0; y < n; y++)
   {
       for(int x=0; x < n; x++)
       {
           cout << "*";
       }
       cout << endl;
   }
   
   
   return 0;
}
