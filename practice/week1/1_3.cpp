#include <iostream>
using namespace std;

int main()
{
   cout << "a: ";
   int a;
   cin >> a;
   cout << "b: ";
   int b;
   cin >> b;
   // Print the highest power of a that is <= b.
   // For example, if a is 10 and b is 123, then you print 100
   // You may assume that a and b are positive.

   int result;
   
   result = a;
   
   while(result <= b)
   {
       // keep multiplying temp by 'a's until it becomes larger than b
       result *= a;
   }
   
   // once it gets out of the while loop, it will hold a value bigger than b
   // therefore, divide by 'a' just one time to have the highest power of a that is <= b
   result /= a;
   
   cout << endl << "Result: " << result << endl;
   return 0;
}
