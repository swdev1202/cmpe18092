#include <iostream>
using namespace std;

int main()
{
   cout << "Hours: ";
   int hours;
   cin >> hours;
   cout << "Minutes: ";
   int minutes;
   cin >> minutes;
   cout << "Minutes to add: ";
   int minutesToAdd;
   cin >> minutesToAdd;

   // Add minutesToAdd minutes to the given hours/minutes
   // and normalize hours/minutes to military time (so that
   // hours is between 0 and 23 and minutes between 0 and 59)
   
   const int TWENTYFOUR = 24;
   const int SIXTY = 60;

   int modifyHour = 0;
   int modifyMinutes = 0;
   int negativeTime = 0; // negative time flag
   int totalMinutes = 0;
   
   if(minutesToAdd < 0)
   {
       // if minutesToAdd is negative
       negativeTime = 1; // raise the negative time flag
       minutesToAdd = (-1 * minutesToAdd); // turn it to poasitive for calculation
   }
   
   modifyHour = minutesToAdd / SIXTY; // get number of hours from the minutes to add
   if(modifyHour > 23)
   {
       modifyHour = modifyHour % TWENTYFOUR;
   }
   
   modifyMinutes = minutesToAdd % SIXTY; // get number of minutes from the minutes to add
   
   if(negativeTime)
   {
       // if we want to subtract the time
       hours = hours - modifyHour;
       if(hours < 0)
       {
           // if hours go below 0,
           hours = TWENTYFOUR + hours;
       }
       
       minutes = minutes - modifyMinutes;
       if(minutes < 0)
       {
           // if minutes go below 0,
           minutes = SIXTY + minutes;
           hours--;
       }
   }
   else
   {
       // if we want to add the time
       hours = hours + modifyHour;
       if(hours > 23)
       {
           // if hours go above 23
           hours = hours - TWENTYFOUR;
       }
       
       minutes = minutes + modifyMinutes;
       if(minutes > 59)
       {
           // if minutes go above 59
           minutes = minutes - SIXTY;
           hours++;
       }
   }
   
   
   cout << endl << "Result: " << hours << ":" << minutes << endl;
   return 0;
}
