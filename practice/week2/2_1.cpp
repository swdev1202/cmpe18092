/**
   Return the nth digit from the back (that is, the factor of pow(10, n)
   in the decimal representation of the given number).
   @param num a number
   @param n the power of 10 for which we want the digit
   @return the nth digit 
   E.g. nthDigit(1729, 1) is 2 since 1729 = 9*pow(10, 0) + 2*pow(10,1) + ...
   Hint: Keep dividing by 10
*/
int nthDigit(int num, int n)
{
    int result;
    while(n >= 0)
    {
        int temp = num; // temp = 1729
        num = num/10; // num = 1729 -> 172
        result = temp - (num * 10); // result = 1729 - 1720 = 9 
        n--;
    }
    
    return result;
}
