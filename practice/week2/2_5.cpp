/**
   We have three t-shirts in sizes small, medium, and large.
   Write a function that takes the weights of three people
   and sets them so that the first parameter becomes
   the weight of the person who should get the small shirt
   (i.e. the lightest one), the second one becomes the weight 
   of the person who should get the medium one (i.e. the
   second-lightest one), and the third parameter becomes
   the weight of the person who should get the largest one.
*/
void smlShirts(int& first, int& second, int& third)
{
    int shirts[3] = {first, second, third};
    int temp;
    if(shirts[0] > shirts[1])
    {
        temp = shirts[1];
        shirts[1] = shirts[0];
        shirts[0] = temp;
    }
    
    if(shirts[0] > shirts[2])
    {
        temp = shirts[2];
        shirts[2] = shirts[0];
        shirts[0] = temp;
    }
    
    if(shirts[1] > shirts[2])
    {
        temp = shirts[2];
        shirts[2] = shirts[1];
        shirts[1] = temp;
    }
    
    first = shirts[0];
    second = shirts[1];
    third = shirts[2];
}
