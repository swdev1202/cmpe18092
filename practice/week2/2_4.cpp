/**
   We have four t-shirts, one of which is extra-large. 
   Write a function that takes the weights of four people
   and returns the weight of the person who should get
   the t-shirt, i.e. the heaviest one.
*/
int xlshirt(int w1, int w2, int w3, int w4)
{
   //find the max
   int weights[4] = {w1, w2, w3, w4};
   
   int heavyWeight = weights[0];
   
   for(int i = 1; i < 4; i++)
   {
       if(weights[i] > heavyWeight)
       {
           heavyWeight = weights[i];
       }
   }
   
   return heavyWeight;
   
}
