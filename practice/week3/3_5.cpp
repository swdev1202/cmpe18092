#include <vector>
using namespace std;

/**
   Computes all rotations of a given vector.
   @param a a vector of integers
   @return a vector of all rotations, first by 0, then by
   one, two, and so on, up to n - 1, where n is the size
   of the vector. For example, if a is {1, 7, 2, 9}
   then an array {{1, 7, 2, 9}, {7, 2, 9, 1}, {2, 9, 1, 7}, {9, 1, 7, 2}} is returned.
*/
vector<vector<int>> allrot(const vector<int>& a)
{
   vector<vector<int>> out;
   
   //out.push_back()
   vector<int> getVec = a;
   
   for(int i = 0; i < a.size(); i++)
   {
       if(i == 0) // just store the first element
       {
           out.push_back(getVec);
       }
       else
       {
           for(int j = 0; j < getVec.size() - 1; j++)
           {
               int temp = getVec.at(j);
               getVec.at(j) = getVec.at(j+1);
               getVec.at(j+1) = temp;
           }
           out.push_back(getVec);
       }
   }
   
   return out;
}
