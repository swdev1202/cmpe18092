#include <iostream>
#include <iomanip>
using namespace std;


/*
  Read numbers from standard input and print the average of 
  all positive numbers, with two digits after the decimal point.
   
  The end of input is indicated by a number 0. For example, 
  if the input is 

  1 2 4 -3 5 -6 0
  
  then you print "Average: 3.00". If there are no positive elements,
  print "Average: 0.00".
*/

int main()
{
   int input;
   int sum = 0;
   int inputCnt = 0;
   
   while(1)
   {
       cin >> input;
       if(input == 0)
       {
           break;
       }
       
       sum += input;
       inputCnt++;
   }
   
   double average = static_cast<double>(sum) / inputCnt;
   
   if(inputCnt == 0)
   {
       cout << "Average: 0.00" << endl;
   }
   else
   {
       //cout << "Average: ";
       cout << fixed << "Average: " << setprecision(2) << average << endl;
   }
   return 0;
}
