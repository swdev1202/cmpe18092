#include <string>
using namespace std;

/**
   Return a string that interleaves the characters in strings a and b.
   If one string is longer than the other, append the suffix.
   For example, splicing "Hello" and "Goodbye" yields "HGeololdobye".
*/
string splice(string a, string b)
{
   int aLength = a.length();
   int bLength = b.length();
   string out = "";
   
   if(aLength == bLength)
   {
       for(int i = 0; i < aLength; i++)
       {
           out += a[i];
           out += b[i];
       }
   }
   else if(aLength > bLength)
   {
       for(int i = 0; i < bLength; i++)
       {
           out += a[i];
           out += b[i];
       }
       out += a.substr(bLength, (aLength-bLength));
   }
   else
   {
       for(int i = 0; i < aLength; i++)
       {
           out += a[i];
           out += b[i];
       }
       out += b.substr(aLength, (bLength-aLength));
   }
   
   return out;
}
