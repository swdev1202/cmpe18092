#include <vector>
using namespace std;

/**
   Rotates a vector one position to the left.
   Returns the rotated vector without modifying the
   original.
   @param a a vector of integers
   @return the rotated vector. For example, if a is {3, 1, 4, 1, 5, 9}
   then a vector {1, 4, 1, 5, 9, 3} is returned.
*/
vector<int> rot(const vector<int>& a)
{
    vector<int> output = a;
    
    if(output.size() < 2)
    {
        return output;
    }
    else
    {
        for(int i = 0; i < (output.size()-1); i++)
        {
            int temp = output.at(i);
            output.at(i) = output.at(i+1);
            output.at(i+1) = temp;
        }
        return output;
    }
}
