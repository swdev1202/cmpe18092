/**
   Computes the average of all positive elements in the given array.
   @param a an array of integers
   @param alen the number of elements in a
   @return the average of all positive elements in a, or 0 if there are none.
*/
double avgpos(int a[], int alen)
{
    int sum = 0;
    int posCnt = 0;
    
    for(int i = 0; i < alen; i++)
    {
        if(a[i] > 0) // if positive
        {
            sum += a[i];
            posCnt++;
        }
    }
    
    if(posCnt == 0) // if there were no positive element in the array
    {
        return 0.0;
    }
    else
    {
        double average = static_cast<double> (sum) / posCnt;
        return average;
    }
}

