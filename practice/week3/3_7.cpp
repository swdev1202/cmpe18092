#include <string.h>
#include <stdio.h>
/**
   Compuute a string that interleaves the characters in strings s and t.
   If one string is longer than the other, append the suffix.
   For example, splicing "Hello" and "Goodbye" yields "HGeololdobye".
   Place the result into the character array r of size rlen.
   If the result does not fit, truncate it to rlen - 1 characters 
   and a '\0' terminator.
*/
void splice(const char s[], const char t[], char r[], int rlen)
{
    for(int i=0; i<rlen; i++)
    {
        r[i] = ' ';
    }
    
    int sLength = strlen(s);
    int tLength = strlen(t);
    
    int sLengthCnt = sLength;
    int tLengthCnt = tLength;

    int rlenCnt = rlen;
    
    int idx = 0;
    
    if(sLength+tLength <= rlen)
    {
        if(sLength == tLength)
        {
            while(sLengthCnt != 0)
            {
                r[idx] = s[idx/2];
                idx++;
                r[idx] = t[(idx-1)/2];
                idx++;
                sLengthCnt--;
                tLengthCnt--;
            }
        }
        else if(sLength > tLength)
        {
            while(tLengthCnt != 0)
            {
                r[idx] = s[idx/2];
                idx++;
                r[idx] = t[(idx-1)/2];
                idx++;
                sLengthCnt--;
                tLengthCnt--;
            }
            
            while(sLengthCnt != 0)
            {
                if((idx%2) == 0) // even number position
                {
                    r[idx] = s[idx/2];
                }
                else
                {
                    r[idx] = s[(idx+1)/2];
                }
                idx++;
                sLengthCnt--;
            }
        }
        else if(tLength > sLength)
        {
            while(sLengthCnt != 0)
            {
                r[idx] = s[idx/2];
                idx++;
                r[idx] = t[(idx-1)/2];
                idx++;
                sLengthCnt--;
                tLengthCnt--;
            }
            
            while(tLengthCnt != 0)
            {
                if((idx%2) == 0) // even number position
                {
                    r[idx] = t[idx/2];
                }
                else
                {
                    r[idx] = t[(idx+1)/2];
                }
                idx++;
                tLengthCnt--;
            }
        }
    }
    else
    {
        if(sLength == tLength)
        {
            while(rlenCnt != 1)
            {
                r[idx] = s[idx/2];
                idx++;
                r[idx] = t[(idx-1)/2];
                idx++;
            }
            // at the end of r, we place /0
            r[idx] = '\0';
        }
        else if(sLength > tLength)
        {
            while(tLengthCnt != 0)
            {
                r[idx] = s[idx/2];
                idx++;
                r[idx] = t[(idx-1)/2];
                idx++;
                sLengthCnt--;
                tLengthCnt--;
                rlenCnt += -2;
            }

            while(rlenCnt != 1)
            {
                if((idx%2) == 0) // even number position
                {
                    r[idx] = s[idx/2];
                }
                else
                {
                    r[idx] = s[(idx+1)/2];
                }
                idx++;
                rlenCnt--;
            }
            r[idx] = '\0';
        }
        else
        {
            while(sLengthCnt != 0)
            {
                r[idx] = s[idx/2];
                idx++;
                r[idx] = t[(idx-1)/2];
                idx++;
                sLengthCnt--;
                tLengthCnt--;
                rlenCnt += -2;
            }

            while(rlenCnt != 1)
            {
                if((idx%2) == 0) // even number position
                {
                    r[idx] = t[idx/2];
                }
                else
                {
                    r[idx] = t[(idx+1)/2];
                }
                idx++;
                rlenCnt--;
            }
            r[idx] = '\0';
        }
    }
}
