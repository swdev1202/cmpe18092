//SOLUTION

#include "path.h"

/*
   Implement the assignment operator!
*/

Path::Path(int n)
{
    // from preceding problem
    xy = new int[2*n];
    length = n;
}

int Path::getX(int i)
{
   if (0 <= i && i < length)
      return xy[2 * i];
   else
      return 0;
}

int Path::getY(int i)
{
   if (0 <= i && i < length)
      return xy[2 * i + 1];
   else
      return 0;
}

void Path::set(int i, int x, int y)
{
   if (0 <= i && i < length)
   {
      xy[2 * i] = x;
      xy[2 * i + 1] = y;
   }
}

int Path::getLength()
{
   return length;
}

Path& Path::operator=(const Path& rhs)
{
  for(int i=0; i<length; i++)
  {
    delete[] xy;
  }

  xy = new int[2*rhs.length];
  length = rhs.length;

  for(int i=0; i<length; i++)
  {
    int x = *(rhs.xy+(2*i));
    int y = *(rhs.xy+(2*i+1));
    set(i, x, y);
  }
}
