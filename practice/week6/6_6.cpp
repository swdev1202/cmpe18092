
#include <algorithm>
#include <sstream>

using namespace std;

/*
  Override the & and + operators!
*/

#include "interval.h"

Interval::Interval()
{
   xmin = 0;
   xmax = 0;
}

Interval::Interval(int xmin, int xmax)
{
   this->xmin = xmin;
   this->xmax = xmax;
}

bool Interval::isEmpty() const
{
   return xmin >= xmax;
}
   
string Interval::toString() const
{
   stringstream sout;
   sout << "[" << xmin << ", " << xmax << ")";
   return sout.str();
}

Interval operator+(const Interval& a, const Interval& b)
{
    int getXmin, getXmax = 0;
   if(a.isEmpty() && !b.isEmpty())
   {
       Interval temp = b;
       return temp;
   }
   else if(!a.isEmpty() && b.isEmpty())
   {
       Interval temp = a;
       return temp;
   }
   else if(a.isEmpty() && b.isEmpty())
   {
       Interval temp;
       return temp;
   }
   else
   {
       if(a.xmin <= b.xmin)
       {
           getXmin = a.xmin;
       }
       else
       {
           getXmin = b.xmin;
       }
       
       if(a.xmax >= b.xmax)
       {
           getXmax = a.xmax;
       }
       else
       {
           getXmax = b.xmax;
       }
       
       Interval temp(getXmin, getXmax);
       return temp;
   }
   
   
}

Interval operator&(const Interval& a, const Interval& b)
{
    int diff_a = a.xmax - a.xmin;
    int diff_b = b.xmax - b.xmin;
    
    if(diff_a == 1 && diff_b == 1)
    {
        Interval temp; // [0,0)
        return temp;
    }
    else
    {
        Interval temp(a.xmin+1,b.xmax-1);
        return temp;
    }
}



