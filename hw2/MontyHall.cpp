/*
 * Author: Seung Won Lee
 * SJSU - FALL 2016
 * CMPE 180-92
 * pracitce 2.b
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <cstdlib>

using namespace std;
typedef int Door;

const int SIMULATION_COUNT = 100;
const bool DEBUG = false;

/**
 * Suggested problem decomposition.
 * You do not have to use these function declarations.
 */

/* 
 * each call will perform 1 monty hall
 * @param sequence - which monty hall simulation
 * @param win1 - keeps track of number of wins when first pick was right
 * @param win2 - keeps track of number of wins when second pick was right
 */
void simulate(int sequence, int& win1, int& win2);

/*
 * randomly picks a door out of door 1, 2, and 3 to hide a car
 * and returns which door Monty hid the car
 */
Door hideCar();

/*
 * After a first choice, Monty opens one door with the goat behind
 * @param firstChoiceDoor - this was the player's first choice
 * @param carBehindDoor - Monty knows where the car is hiding
 * return remaining door
 */
Door openDoor(Door firstChoiceDoor, Door carBehindDoor);

/*
 * Player randomly picks a door out of door 1, 2, and 3.
 */
Door makeFirstChoice();

/*
 * After one door is opened, player choses the second door
 * @param firstDoor - the first door that player originally picked
 * @param openedDoor - the door that was opened by Monty
 * return the second choice door
 */
Door makeSecondChoice(Door firstDoor, Door openedDoor);


/*
 * randomly selects a door from 1, 2, and 3
 * and returns the door it picked
 */
Door randomDoor();

/*
 * It returns a door number other than those two given doors.
 */
Door randomDoorNot(Door aDoor, Door anotherDoor);

/*
 * it selects the remaining door.
 */
Door chooseRemainingDoor(Door firstDoor, Door openedDoor);

/*
 * print the header information
 */
void printHeader();

/**
 * Main
 */
int main()
{
    int win1 = 0, win2 = 0;

    srand(time(NULL));  // seed the random number generator
    printHeader();

    // Run the simulations.
    for (int i = 1; i <= SIMULATION_COUNT; i++) simulate(i, win1, win2);

    cout << endl;
    cout << setw(4) << win1 << " wins if stay with the first choice" << endl;
    cout << setw(4) << win2 << " wins if switch to the second choice" << endl;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(1);

    cout << endl;
    cout << "Win ratio of switch over stay: ";
    cout << static_cast<double>(win2)/win1 << endl;
}

/***** Complete this program. ****/
void simulate(int sequence, int& win1, int& win2)
{
    // First, print which simulation it is
    cout << setw(4) << sequence;

    // 1. Monty hides a car.
    Door carHidden = hideCar();
    cout << setw(8) << carHidden;

    // 2. Player makes the first choice.
    Door firstChoice = makeFirstChoice();
    cout << setw(8) << firstChoice;

    // 3. Choose which door Monty opens
    // Monty has to...
    //      1. avoid opening the first choice door
    //      2. the door with the hidden car
    Door openedDoor = openDoor(firstChoice, carHidden);
    cout << setw(8) << openedDoor;

    // 4. Player makes the second choice.
    // Player just needs to pick the remaining door that is not openend
    Door secondChoice = makeSecondChoice(firstChoice, openedDoor);
    cout << setw(8) << secondChoice;

    // 5. Figure out if first or second choice wins
    if(carHidden == firstChoice)
    {
        win1++;
        cout << setw(8) << "yes";
    }
    else if(carHidden == secondChoice)
    {
        win2++;
        cout << setw(16) << "yes";
    }
    else
    {
        cout << "Error: this cannot happen." << endl;
    }

    // debug purpose
    if(DEBUG)
    {
        if(firstChoice == openedDoor) { cout << setw(8) << "First and Open Cannot be the same";}
        if(carHidden == openedDoor) { cout << setw(8) << "Hidden and Open Cannot be the same";}
        if(openedDoor == secondChoice) { cout << setw(8) << "Open and Second Cannot be the same";}
    }

    cout << endl;
}

Door hideCar()
{
    return randomDoor();
}

Door openDoor(Door firstChoiceDoor, Door carBehindDoor)
{
    // when the door with the car and the player's pick is the same
    // open any door with a goat behind
    if(firstChoiceDoor == carBehindDoor)
    {
        if(carBehindDoor == 1)
        {
            // choose between 2 or 3
            return (rand() % 2) + 2;
        }
        else if(carBehindDoor == 2)
        {
            // choose between 1 or 3
            Door temp;
            do
            {
                temp = randomDoor();
            }  
            while(temp == 2);
        }
        else
        {
            // choose between 1 or 2
            return (rand() % 2) + 1;
        }
    }
    else
    {
        // choose a door that is neither a firstchoiceDoor and a carbehindDoor
        return randomDoorNot(firstChoiceDoor, carBehindDoor);
    }
}

Door makeFirstChoice()
{
    return randomDoor();
}

Door makeSecondChoice(Door firstDoor, Door openDoor)
{
    return chooseRemainingDoor(firstDoor, openDoor);
}

Door randomDoor()
{
    return (rand() % 3) + 1;
}

Door randomDoorNot(Door aDoor, Door anotherDoor)
{
    return chooseRemainingDoor(aDoor, anotherDoor);
}

Door chooseRemainingDoor(Door firstDoor, Door openedDoor)
{
    //add those two door numbers
    Door sum = firstDoor + openedDoor;
    // chose the remaining closed door and return its number
    if(sum == 3) // 1+2
    {
        return 3;
    }
    else if(sum == 4) // 1+3
    {
        return 2;
    }
    else if(sum == 5) // 2+3
    {
        return 1;
    }
}

void printHeader()
{
    cout << setw(4) << "#" 
         << setw(8) << "Car"
         << setw(8) << "First"
         << setw(8) << "Opened" 
         << setw(8) << "Second" 
         << setw(8) << "Win" 
         << setw(8) << "Win" 
         << endl;
    cout << setw(4) << "" 
         << setw(8) << "Here"
         << setw(8) << "choice"
         << setw(8) << "door" 
         << setw(8) << "choice" 
         << setw(8) << "first" 
         << setw(8) << "second" 
         << endl;
}