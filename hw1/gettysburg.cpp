#include <iostream>
#include <fstream>
#include <string>

using namespace std;

const string INPUT_FILE_NAME = "GettysburgAddress.txt";

int main()
{
    string line;
    int line_count = 0;
    int character_count = 0;
    int word_count = 0;
    int lower_count = 0;
    int upper_count = 0;
    int space_count = 0;
    int punctuation_count = 0;

    ifstream input;
    input.open(INPUT_FILE_NAME.c_str());
    if (input.fail())
    {
        cout << "Failed to open " << INPUT_FILE_NAME << endl;
        return -1;
    }

    cout << "Statistics for file " << INPUT_FILE_NAME << ":" << endl;
    cout << endl;

    /***** Complete this program. *****/

    int was_it_a_letter = 0;

	while(getline(input, line))
	{
		line_count++; // counts number of lines in a txt file
		character_count += line.length(); // counts number of chracters
		for(int i = 0; i < line.length(); i++)
		{
			char getchar = line[i]; // get each letter in a string
			int char_to_ascii = (int)getchar; // convert each letter into a ascii code
			// find the upper case letter
			if(char_to_ascii >= 65 && char_to_ascii <= 90) // if ascii number is A - Z
			{
				upper_count++;	// increment upper case letter counts
				was_it_a_letter = 1;
			}
			else if(char_to_ascii >= 97 && char_to_ascii <= 122) // if ascii code is a - z
			{
				lower_count++;	// increment lower case letter counts
				was_it_a_letter = 1;
			}
			else if(char_to_ascii == 32) // if ascii number is space
			{
				space_count++; // increment space character counts
				if(was_it_a_letter) // check if the previous character was a letter
				{
					word_count++;
					was_it_a_letter = 0; // reset
				}
			}
			else // if ascii number is not corresponding to any of them, consider it as a punctuation
			{
				punctuation_count++; // increment punctuation mark counts
				if(was_it_a_letter)
				{
					word_count++;
					was_it_a_letter = 0;
				}
			}
		}
		// at the end of the line
		if(was_it_a_letter)
		{
			word_count++;
			was_it_a_letter = 0;
		}
	}

    cout << "Lines: " << line_count << endl;
    cout << "Characters: " << character_count << endl;
    cout << "Words: " << word_count << endl;
    cout << "Lower-case letters: " << lower_count << endl;
    cout << "Upper-case letters: " << upper_count << endl;
    cout << "Spaces: " << space_count << endl;
    cout << "Punctuation marks: " << punctuation_count << endl;
}

