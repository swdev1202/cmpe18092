/**
 * Test the binary search tree and AVL tree implementations.
 * The AVL tree is derived from the binary search tree.
 *
 * Create a tree of height 5 and then repeatedly
 * delete the root. The AVL tree should remain balanced
 * after each node insertion and deletion.
 *
 * Author: Ron Mak
 *         Department of Computer Engineering
 *         San Jose State University
 */
#include <iostream>
#include <vector>
#include <ctime>
#include <chrono>
#include <time.h>
#include <vector>

#include "BinarySearchTree.h"
#include "AvlTree.h"
#include "BinaryTreeChecker.h"
#include "BinaryTreePrinter.h"

using namespace std;
using namespace std::chrono;

const bool DUMP = false;

void testBST(BinarySearchTree<int>& tree, vector<int>* data);
void testAVL(AvlTree<int>& tree, vector<int>* data);

void makeTree(BinarySearchTree<int>& tree,
              BinaryTreeChecker<int>& checker,
              BinaryTreePrinter<int>& printer,
              string kind,
              vector<int>* data);

void testTree(BinarySearchTree<int>& tree,
              BinaryTreeChecker<int>& checker,
              BinaryTreePrinter<int>& printer);

void searchTree(BinarySearchTree<int>& tree, vector<int>* data);

/**
 * Main.
 */
int main( )
{
	srand(time(NULL));

	static const int N[] = {10000, 20000, 30000, 40000, 50000,
 						60000, 70000, 80000, 90000, 100000};

 	/*static const int N[] = {100000, 200000, 300000, 400000, 500000,
 							600000, 700000, 800000, 900000, 1000000};
*/

	for(int i : N)
	{
		vector<int>* dataN = new vector<int>();
		for(int j=0; j<i; j++)
		{
			int value = rand()%i;
			dataN->push_back(value);
		}

		BinarySearchTree<int> bstTree;
		AvlTree<int> avlTree;

		steady_clock::time_point bstInsert_startTime = steady_clock::now();
		testBST(bstTree, dataN);
		steady_clock::time_point bstInsert_endTime = steady_clock::now();
		
		steady_clock::time_point avlInsert_startTime = steady_clock::now();
		//testAVL(avlTree, dataN);
		steady_clock::time_point avlInsert_endTime = steady_clock::now();

		long bstInsert_elapsedTime = duration_cast<milliseconds>(bstInsert_endTime - bstInsert_startTime).count();
		long avlInsert_elapsedTime = duration_cast<milliseconds>(avlInsert_endTime - avlInsert_startTime).count();
		cout << "Data Size = " << i << endl;
		cout << "BST Insertion Time = " << bstInsert_elapsedTime << "ms" << endl;
		cout << "AVL Insertion Time = " << avlInsert_elapsedTime << "ms" << endl;

		delete dataN;

		vector<int>* searchN = new vector<int>();
		for(int j=0; j<i; j++)
		{
			int value = rand()%i;
			searchN->push_back(value);
		}

		steady_clock::time_point bstSearch_startTime = steady_clock::now();
		searchTree(bstTree, searchN);
		steady_clock::time_point bstSearch_endTime = steady_clock::now();

		steady_clock::time_point avlSearch_startTime = steady_clock::now();
		//searchTree(avlTree, searchN);
		steady_clock::time_point avlSearch_endTime = steady_clock::now();

		long bstSearch_elapsedTime = duration_cast<milliseconds>(bstSearch_endTime - bstSearch_startTime).count();
		long avlSearch_elapsedTime = duration_cast<milliseconds>(avlSearch_endTime - avlSearch_startTime).count();
		cout << "BST Search Time = " << bstSearch_elapsedTime << "ms" << endl;
		cout << "AVL search Time = " << avlSearch_elapsedTime << "ms" << endl;
		cout << endl;

		delete searchN;
	}
}


void searchTree(BinarySearchTree<int>& tree, vector<int>* data)
{
	for(int i=0; i<(*data).size(); i++)
	{
		tree.contains((*data).at(i));
	}
}


/**
 * Run the test with a binary search tree.
 */
void testBST(BinarySearchTree<int>& tree, vector<int>* data)
{
    //BinarySearchTree<int>  tree;
    BinaryTreeChecker<int> checker(tree);
    BinaryTreePrinter<int> printer(tree);
	makeTree(tree, checker, printer, "BST", data);
}

/**
 * Run the test with an AVL tree.
 */
void testAVL(AvlTree<int>& tree, vector<int>* data)
{
    //AvlTree<int> tree;
    BinaryTreeChecker<int> checker(tree);
    BinaryTreePrinter<int> printer(tree);
    makeTree(tree, checker, printer, "AVL", data);

}

/**
 * Make a binary search tree containing unique random integer data items.
 * @param tree the tree to make.
 * @param maxNodes the maximum number of nodes to generate.
 * @param checker the binary tree checker to use.
 * @param printer the binary tree printer to use.
 * @param insertPrint true iff print a message after each node insertion
 */
void makeTree(BinarySearchTree<int>& tree,
              BinaryTreeChecker<int>& checker,
              BinaryTreePrinter<int>& printer,
              string kind,
              vector<int>* data)
{
    for (int i=0; i<(*data).size(); i++)
    {
        tree.insert((*data).at(i));
    }
}

/**
 * Test a binary tree.
 * @param tree the tree to test.
 * @param checker the binary tree checker to use.
 * @param printer the binary tree printer to use.
 */
void testTree(BinarySearchTree<int>& tree,
              BinaryTreeChecker<int>& checker,
              BinaryTreePrinter<int>& printer)
{
    int status = BinaryTreeChecker<int>::NO_ERROR;

    // Delete the root node each time through the loop.
    // Print the tree after each deletion.
    while (!tree.isEmpty())
    {
        BinaryNode<int> *root = tree.getRoot();
        int data = root->data;
        cout << endl << "Deleted root node " << data << ":" << endl;

        tree.remove(data);
        checker.remove(data);

        cout << endl;
        printer.print();

        status = checker.check(DUMP);
        if (status != BinaryTreeChecker<int>::NO_ERROR) break;
    }

    // What was the status?
    string msg;
    switch (status)
    {
        case BinaryTreeChecker<int>::NO_ERROR:
        {
            msg = ">>>>> The tree is now empty.";
            break;
        }
        case BinaryTreeChecker<int>::DATA_MISMATCH:
        {
            msg =  ">>>>> Data mismatch.";
            break;
        }
        case BinaryTreeChecker<int>::INSUFFICIENT_DATA:
        {
            msg =  ">>>>> Data missing from tree.";
            break;
        }
        case BinaryTreeChecker<int>::REMAINING_DATA:
        {
            msg =  ">>>>> Data remaining in tree.";
            break;
        }
    }
    cout << msg << endl;
}