Data Size = 10000
BST Insertion Time = 5ms
AVL Insertion Time = 3042ms
BST Search Time = 2ms
AVL search Time = 2ms

Data Size = 20000
BST Insertion Time = 7ms
AVL Insertion Time = 12313ms
BST Search Time = 4ms
AVL search Time = 3ms

Data Size = 30000
BST Insertion Time = 9ms
AVL Insertion Time = 23919ms
BST Search Time = 14ms
AVL search Time = 17ms

Data Size = 40000
BST Insertion Time = 26ms
AVL Insertion Time = 45159ms
BST Search Time = 10ms
AVL search Time = 14ms

Data Size = 50000
BST Insertion Time = 20ms
AVL Insertion Time = 69205ms
BST Search Time = 25ms
AVL search Time = 23ms

Data Size = 60000
BST Insertion Time = 30ms
AVL Insertion Time = 99172ms
BST Search Time = 42ms
AVL search Time = 24ms

Data Size = 70000
BST Insertion Time = 45ms
AVL Insertion Time = 140311ms
BST Search Time = 34ms
AVL search Time = 19ms

Data Size = 80000
BST Insertion Time = 29ms
AVL Insertion Time = 182735ms
BST Search Time = 50ms
AVL search Time = 30ms

Data Size = 90000
BST Insertion Time = 50ms
AVL Insertion Time = 239941ms
BST Search Time = 43ms
AVL search Time = 24ms

Data Size = 100000
BST Insertion Time = 44ms
AVL Insertion Time = 304883ms
BST Search Time = 48ms
AVL search Time = 28ms


