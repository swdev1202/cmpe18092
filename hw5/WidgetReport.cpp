#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

class WidgetReport
{

public:
    WidgetReport(ifstream& in) : input (in) {lineNum = 0; totalCntDept = 0; totalCntPlant = 0;
                                             totalCntState = 0; grandTotal = 0;}
    void processLine();

    void printLine();
    void printHeader();
    void printTotalDept(int in_deptNum);
    void printTotalPlant(int in_plantNum);
    void printTotalState(int in_stateNum);
    void printGrandTotal();

    int printMidReport(bool dept, bool plant, bool state);
    
    int getState() {return stateNum;}
    int getPlant() {return plantNum;}
    int getDept() {return deptNum;}
    int getEmpid() {return empidNum;}
    int getCount() {return countNum;}
    int getLineNum() {return lineNum;}

    void addTotalCntDept(int deptCount) {totalCntDept+= deptCount;}
    void addTotalCntPlant(int plantCount) {totalCntPlant += plantCount;}
    void addTotalCntState(int stateCount) {totalCntState += stateCount;}
    void addGrandTotal(int Count) {grandTotal += Count;}

    void initTotalCntDept() {totalCntDept = 0;}
    void initTotalCntPlant() {totalCntPlant = 0;}
    void initTotalCntState() {totalCntState = 0;}


private:
    ifstream& input;  // reference to the input stream
    int stateNum;     // store the state number
    int plantNum;     // store the plant number
    int deptNum;      // store the department number
    int empidNum;     // store the empid number
    int countNum;     // store the count number
    string name;

    int lineNum;

    int totalCntDept;
    int totalCntPlant;
    int totalCntState;
    int grandTotal;

    
};

const string INPUT_FILE_NAME = "widgets.txt";

int main()
{
    ifstream input;
    input.open(INPUT_FILE_NAME);
    
    if (input.fail())
    {
        cout << "Failed to open " << INPUT_FILE_NAME << endl;
        return -1;
    }

    // create a WidgetReport class object "report" by passing an ifstream input
    WidgetReport report(input);

    int state, plant, dept, empid, count;
    int lineNum;
    int stopFlag = 0;

    while(1)
    {
        report.processLine();
        lineNum = report.getLineNum();

        if(lineNum == 1)
        {
            report.printHeader();
        }
        else if(lineNum == 14)
        {
            break;
        }
        else if(lineNum == 2) // the very first employee
        {
            // store into local variables
            state = report.getState();
            plant = report.getPlant();
            dept = report.getDept();
            empid = report.getEmpid();
            count = report.getCount();

            report.addTotalCntDept(count); // add to the total department count
            report.addTotalCntPlant(count); // add to the total plant count
            report.addTotalCntState(count); // add to the total sate count
            report.addGrandTotal(count);
            report.printLine();
        }
        else
        {
            int diffDept = report.getDept() - dept;
            int diffPlant = report.getPlant() - plant;
            int diffState = report.getState() - state;

            bool deptFlag = false;
            bool plantFlag = false;
            bool stateFlag = false;

            // there is a change in department number
            if(diffDept != 0) {deptFlag = true;}
            else {deptFlag = false;}

            // there is a change in plant number
            if(diffPlant != 0) {plantFlag = true;}
            else {plantFlag = false;}

            // there is a change in state number
            if(diffState != 0) {stateFlag = true;}
            else {stateFlag = false;}
            
            int status = report.printMidReport(deptFlag, plantFlag, stateFlag);

            if(status == 0)
            {
                // no change in dept, plant, and state number
            }
            else if(status == 1)
            {
                // print dept change
                report.printTotalDept(dept);
                report.initTotalCntDept();
                cout << endl;
            }
            else if(status == 2)
            {
                // print plant change
                report.printTotalDept(dept);
                report.initTotalCntDept();
                report.printTotalPlant(plant);
                report.initTotalCntPlant();
                cout << endl;
            }
            else if(status == 3)
            {
                // print state change
                report.printTotalDept(dept);
                report.initTotalCntDept();
                report.printTotalPlant(plant);
                report.initTotalCntPlant();
                report.printTotalState(state);
                report.initTotalCntState();
                cout << endl;
            }
            else
            {
                cout << "cannot happen" << endl;
            }

            report.printLine();
            // store them so we can use this data at the next line
            state = report.getState();
            plant = report.getPlant();
            dept = report.getDept();
            empid = report.getEmpid();
            count = report.getCount();

            report.addTotalCntDept(count); // add to the total department count
            report.addTotalCntPlant(count); // add to the total plant count
            report.addTotalCntState(count); // add to the total sate count
            report.addGrandTotal(count); // add to the grand total
        }
    }

    report.printTotalDept(dept);
    report.printTotalPlant(plant);
    report.printTotalState(state);
    report.printGrandTotal();

    input.close();
    return 0;
}

/***** Put member function definitions after the main. *****/
void WidgetReport::processLine()
{
    string line = "";
    getline(input, line);
    lineNum++;

    int columnCnt = 0;
    string temp = "";

    if(lineNum > 1)
    {
        for(int i=0; i < line.length(); i++)
        {
            if(line[i] == ' ') // if the current character is a space
            {
                columnCnt++; // found a value to store
                if(columnCnt == 1)
                {
                    // state number
                    stateNum = stoi(temp);
                    temp = "";
                }
                else if(columnCnt == 2)
                {
                    // plant number
                    plantNum = stoi(temp);
                    temp = "";
                }
                else if(columnCnt == 3)
                {
                    // department number
                    deptNum = stoi(temp);
                    temp = "";
                }
                else if(columnCnt == 4)
                {
                    // empid number
                    empidNum = stoi(temp);
                    temp = "";
                }
                else if(columnCnt == 5)
                {
                    // first name
                    temp += line[i];
                }
                else if(columnCnt == 6)
                {
                    // last name
                    name = temp;
                    temp = "";
                }
                else
                {
                    // none
                }
            }
            else
            {
                temp += line[i];
            }

            if(i == line.length() - 1)
            {
                // if it reached at the end of the line
                countNum = stoi(temp);
            }
        }
    }
}

void WidgetReport::printLine()
{
    cout <<  setw(5) << stateNum << " ";
    cout << setw(5) << plantNum << " ";
    cout << setw(5) << deptNum << " ";
    cout << setw(5) << empidNum << " ";
    cout << setw(5) << countNum << " ";
    cout << name << " ";
    cout << endl;
}

void WidgetReport::printHeader()
{
    cout << "STATE " << "PLANT " << "DEPT " << "EMPID " << "COUNT " << "NAME " << endl;
    cout << endl; // extra line to match the output format
}

void WidgetReport::printTotalDept(int in_deptNum)
{
    cout << endl;
    cout << setw(29) << totalCntDept << " TOTAL FOR DEPT" << setw(4) << in_deptNum;
    cout << " *" << endl;
}

void WidgetReport::printTotalPlant(int in_plantNum)
{
    cout << setw(29) << totalCntPlant << " TOTAL FOR PLANT" << setw(3) << in_plantNum;
    cout << " **" << endl;
}

void WidgetReport::printTotalState(int in_stateNum)
{
    cout << setw(29) << totalCntState << " TOTAL FOR STATE" << setw(3) << in_stateNum;
    cout << " ***" << endl;
}

void WidgetReport::printGrandTotal()
{
    cout << endl;
    cout << setw(29) << grandTotal << " GRAND TOTAL" << setw(12) << " ****" << endl;
}

int WidgetReport::printMidReport(bool dept, bool plant, bool state)
{
    if(!state && !plant && !dept) // 000
    {
        // no change in the dept, plant and state number
        return 0;
    }
    else if(!state && !plant && dept) // 001
    {
        // there was a change in dept
        return 1;
    }
    else if(!state && plant && !dept) // 010
    {
        // there was a change in plant
        return 2;
    }
    else if(!state && plant && dept) // 011
    {
        // there was a change in plant & dept...
        return 2;
    }
    else
    {
        // 100, 101, 110, 111
        // there was a change in state
        return 3;
    }
}