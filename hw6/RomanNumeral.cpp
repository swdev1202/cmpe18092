#include "RomanNumeral.h"
#include <iostream>

using namespace std;

/*
 * Default constructor.
 */
RomanNumeral::RomanNumeral() : roman(""), decimal(0)
{
}

/*
 * constructor with a pre-determined string
 */
RomanNumeral::RomanNumeral(string roman)
{
	this->roman = roman;
	decimal = 0;
	toDecimal();
}

/*
 * constructor with a pre-determined value
 */
RomanNumeral::RomanNumeral(int value)
{
	decimal = value;
	roman = "";
	toRoman();
}

/*
 * a public function that returns a current roman numeral in string
 */
string RomanNumeral::getRomanString()
{
	return roman;
}

/*
 * a public function that returns a current roman numeral in decimal
 */
int RomanNumeral::getRomanDecimal()
{
	return decimal;
}

/*
 * overloading operator + to add two roman numeral numbers
 */
RomanNumeral operator+(const RomanNumeral& lhs, const RomanNumeral& rhs)
{
	int sum = lhs.decimal + rhs.decimal;
	RomanNumeral temp(sum);
	return temp;
}

/*
 * overloading operator - to subtract two roman numeral numbers
 */
RomanNumeral operator-(const RomanNumeral& lhs, const RomanNumeral& rhs)
{
	int sub = lhs.decimal - rhs.decimal;
	RomanNumeral temp(sub);
	return temp;
}

/*
 * overloading operator * to multiply two roman numeral numbers
 */
RomanNumeral operator*(const RomanNumeral& lhs, const RomanNumeral& rhs)
{
	int multiply = lhs.decimal * rhs.decimal;
	RomanNumeral temp(multiply);
	return temp;
}

/*
 * overloading operator / to divide two roman numeral numbers
 */
RomanNumeral operator/(const RomanNumeral& lhs, const RomanNumeral& rhs)
{
	int division = lhs.decimal / rhs.decimal;
	RomanNumeral temp(division);
	return temp;
}

/*
 * overloading operator == to compare two roman numerals and return true or false
 */
bool operator==(const RomanNumeral& lhs, const RomanNumeral& rhs)
{
	if(lhs.decimal == rhs.decimal)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
 * overloding operator != to compare two roman numerals and return true if not equal
 */
bool operator!=(const RomanNumeral& lhs, const RomanNumeral& rhs)
{
	if(lhs.decimal != rhs.decimal)
	{
		return true;
	}
	else
	{
		return false;
	}
}



/*
 * overloading operator << to print in [integer value:roman string]
 */
ostream& operator<<(ostream& outs, const RomanNumeral& romanNum)
{
	outs << "[" << romanNum.decimal << ":" << romanNum.roman << "]";
	return outs;
}

/*
 * overloading operator >> to take the input stream
 */
istream& operator>>(istream& ins, RomanNumeral& romanNum)
{
	ins >> romanNum.roman;
	romanNum.decimal = 0;
	romanNum.toDecimal();
	return ins;
}

/*
 * a private function that converts a decimal number to a roman number
 */
void RomanNumeral::toRoman()
{
	int one = 0;
	int ten = 0;
	int hundred = 0;
	int thousand = 0;

	if(decimal >= 1 && decimal < 10)
	{
		one = decimal;
	}
	else if(decimal >= 10 && decimal < 100)
	{
		ten = (decimal/10);
		one = decimal - (ten*10);
	}
	else if(decimal >= 100 && decimal < 1000)
	{
		hundred = (decimal/100);
		ten = (decimal - (hundred*100)) / 10;
		one = (decimal - (hundred*100) - (ten*10));
	}
	else if(decimal >= 1000 && decimal < 10000)
	{
		thousand = (decimal/1000);
		hundred = (decimal - (thousand*1000)) / 100;
		ten = (decimal - (thousand*1000) - (hundred*100)) / 10;
		one = (decimal - (thousand*1000) - (hundred*100) - (ten*10)); 
	}
	else
	{
		one = -1;
		ten = -1;
		hundred = -1;
		thousand = -1;
	}

	// thousand th convert
	if(thousand >= 1) // if there is at least one thousand
	{
		for(int i=0; i<thousand; i++)
		{
			roman += "M";
		}
	}

	// hundredth convert
	if(hundred == 0) // 000
	{
		// do nothing
	}
	else if((hundred >= 1 && hundred <= 4) || hundred >= 9) // 100, 200, 300, 400, 900
	{
		roman += "C"; // the first roman is C
		if(hundred == 2 || hundred == 3) // 200, 300
		{
			for(int i=1; i<hundred; i++)
			{
				roman += "C";
			}
		}
		else if(hundred == 4)
		{
			roman += "D";
		}
		else if(hundred == 9)
		{
			roman += "M";
		}
	}
	else // 500, 600, 700, 800
	{
		roman += "D"; // the first roman is D
		for(int i=5; i<hundred; i++)
		{
			roman += "C";
		}
	}

	// tenth convert
	if(ten == 0) // 00
	{
		// do nothing
	}
	else if((ten >= 1 && ten <= 4) || ten >= 9) // 10, 20, 30, 40, 90
	{
		roman += "X"; // the first roman is X
		if(ten == 2 || ten == 3) // 20, 30
		{
			for(int i=1; i<ten; i++)
			{
				roman += "X";
			}
		}
		else if(ten == 4)
		{
			roman += "L";
		}
		else if(ten == 9)
		{
			roman += "C";
		}
	}
	else // 50, 60 ,70 ,80
	{
		roman += "L"; // the first roman is D
		for(int i=5; i<ten; i++)
		{
			roman += "X";
		}
	}

	// one's convert
	if(one == 0) // 0
	{
		//do nothing
	}
	else if((one >= 1 && one <= 4) || one >= 9) // 1, 2, 3, 4, 9
	{
		roman += "I"; // the first roman is I
		if(one == 2 || one == 3) // 2, 3
		{
			for(int i=1; i<one; i++)
			{
				roman += "I";
			}
		}
		else if(one == 4)
		{
			roman += "V";
		}
		else if(one == 9)
		{
			roman += "X";
		}
	}
	else // 5, 6, 7, 8
	{
		roman += "V";
		for(int i=5; i<one; i++)
		{
			roman += "I";
		}
	}
}

/*
 * a private function that converts a roman number to a decimal number
 */
void RomanNumeral::toDecimal()
{
	char prev = ' '; // dummy value
	for(int i=0; i < roman.length(); i++)
	{
		char c = roman[i];

		if(c == 'I')
		{
			decimal += 1;
		}
		else if(c == 'V')
		{
			if(prev == 'I') // IV
			{
				decimal += 3;
			}
			else
			{
				decimal += 5;
			}
		}
		else if(c == 'X')
		{
			if(prev == 'I') // IX
			{
				decimal += 8;
			}
			else
			{
				decimal += 10;
			}
		}
		else if(c == 'L')
		{
			if(prev == 'X') // XL
			{
				decimal += 30;
			}
			else
			{
				decimal += 50;
			}
		}
		else if(c == 'C')
		{
			if(prev == 'X') // XC
			{
				decimal += 80;
			}
			else
			{
				decimal += 100;
			}
		}
		else if(c == 'D')
		{
			if(prev == 'C') // CD
			{
				decimal += 300;
			}
			else
			{
				decimal += 500;
			}
		}
		else if(c == 'M')
		{
			if(prev == 'C') // CM
			{
				decimal += 800;
			}
			else
			{
				decimal += 1000;
			}
		}
		else // cannot happen
		{
			decimal = -1;
		}

		prev = c;
	}
}