#ifndef ROMANNUMERAL_H_
#define ROMANNUMERAL_H_

#include <string>

using namespace std;

class RomanNumeral
{
public:
    RomanNumeral();
    RomanNumeral(string roman);
    RomanNumeral(int value);
    
    string getRomanString();
    int getRomanDecimal();

    friend RomanNumeral operator+(const RomanNumeral& lhs, const RomanNumeral& rhs);
    friend RomanNumeral operator-(const RomanNumeral& lhs, const RomanNumeral& rhs);
    friend RomanNumeral operator*(const RomanNumeral& lhs, const RomanNumeral& rhs);
    friend RomanNumeral operator/(const RomanNumeral& lhs, const RomanNumeral& rhs);

    friend bool operator==(const RomanNumeral& lhs, const RomanNumeral& rhs);
    friend bool operator!=(const RomanNumeral& lhs, const RomanNumeral& rhs);

    friend ostream& operator<<(ostream& outs, const RomanNumeral& romanNum);
    friend istream& operator>>(istream& ins, RomanNumeral& romanNum);

private:
    string roman;      // Roman numeral as a string
    int    decimal;    // decimal value of the Roman numeral

    void toRoman();    // calculate string from decimal value
    void toDecimal();  // calculate decimal value from string
};

#endif /* ROMANNUMERAL_H_ */
