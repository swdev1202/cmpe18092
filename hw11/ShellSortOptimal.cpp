/*
 * Assignment #12: Compare sorting algorithms
 *
 * CMPE 180-92 Data Structures and Algorithms in C++
 * Department of Computer Engineering
 * R. Mak, Nov. 20, 2016
 */
#include "ShellSortOptimal.h"

/**
 * Default constructor.
 */
ShellSortOptimal::ShellSortOptimal() {}

/**
 * Destructor.
 */
ShellSortOptimal::~ShellSortOptimal() {}

/**
 * Get the name of this sorting algorithm.
 * @return the name.
 */
string ShellSortOptimal::name() const { return "Shellsort optimal"; }

/**
 * Run the optimal shellsort algorithm.
 * According to Don Knuth:
 * h = 3*(i-1) + 1 for i = 0, 1, 2, ... used in reverse.
 * @throws an exception if an error occurred.
 */
void ShellSortOptimal::run_sort_algorithm() throw (string)
{
	vector<int> h;
	int jump = 1;
	h.push_back(jump);
	while(1)
	{
		jump = (3*jump) + 1;
		if(jump > size/2)
		{
			break;
		}
		else
		{
			h.push_back(jump);
		}
	}

	vector<int>::reverse_iterator it;
	for(it = h.rbegin(); it != h.rend(); it++)
	{
		int jumpVal = *it;
		for(int i=jumpVal; i<size; i++)
		{
			int currElement = data[i];
			int j;
			compare_count++;
			for(j = i; j >= jumpVal && data[j-jumpVal] > currElement; j -= jumpVal)
			{
				move_count++;
				data[j] = data[j-jumpVal];
			}
			if(j == i)
			{
				// not moved
			}
			else
			{
				move_count++;
				data[j] = currElement;
			}
		}
	}
}