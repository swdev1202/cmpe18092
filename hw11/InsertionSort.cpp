/*
 * Assignment #12: Compare sorting algorithms
 *
 * CMPE 180-92 Data Structures and Algorithms in C++
 * Department of Computer Engineering
 * R. Mak, Nov. 20, 2016
 */
#include "InsertionSort.h"

/**
 * Default constructor.
 */
InsertionSort::InsertionSort() : VectorSorter() {}

/**
 * Destructor.
 */
InsertionSort::~InsertionSort() {}

/**
 * Get the name of this sorting algorithm.
 * @return the name.
 */
string InsertionSort::name() const { return "Insertion sort"; }

/**
 * Run the insertion sort algorithm.
 * @throws an exception if an error occurred.
 */
void InsertionSort::run_sort_algorithm() throw (string)
{
    /***** Complete this member function. *****/

	/*
    // For each element of the vector ...
    for(int i = 1; i < size; i++)
    {
    	int currentElement = data[i];

    	// compare if the previous element is smaller than the current element
    	compare_count++;
    	if(currentElement >= data[i-1])
    	{
    		// proceed
    	}
    	else
    	{
    		// if the previous one is larger than the current element
    		// look for a place to store this current item
    		for(int j = 0; j < i; j++)
    		{
    			compare_count++;

    			if(currentElement < data[j])
    			{
    				// is this element bigger than my current element?
    				// we have found a place to store my element!
    				for(int k = i; k > j; k--)
    				{
    					data[k] = data[k-1];
    					move_count++;
    				}
    				data[j] = currentElement; // place it
    				move_count++;
    				break;
    			}
    			else
    			{
    				// proceed
    			}
    		}
    	}
    }*/
    /*
     * 
     */
    //print_data();
    for(int i=1; i<size; i++)
    {
    	// compare to the previous element
    	int j = i;

    	while(j > 0)
    	{
    		compare_count++;
    		// compare the current indexed element to the previous element
    		if(data[j] < data[j-1])
    		{
    			//if the previous element is larger, swap it
    			swap(j,j-1);
    			//print_data();
    		}
    		else
    		{
    			// it found the right position to be inserted
    			break;
    		}
    		j--;
    	}
    }
}
