/*
 * Assignment #12: Compare sorting algorithms
 *
 * CMPE 180-92 Data Structures and Algorithms in C++
 * Department of Computer Engineering
 * R. Mak, Nov. 20, 2016
 */
#include "QuickSorter.h"
#include <iostream>



/**
 * Default constructor.
 */
QuickSorter::QuickSorter() : VectorSorter() {}

/**
 * Destructor.
 */
QuickSorter::~QuickSorter() {}

/**
 * Run the quicksort algorithm.
 * @throws an exception if an error occurred.
 */
void QuickSorter::run_sort_algorithm() throw (string)
{
    quicksort(0, size-1);
}

/**
 * The quicksort algorithm recursively sorts data subranges.
 * @param left the left index of the subrange to sort.
 * @param right the right index of the subrange to sort.
 */
void QuickSorter::quicksort(const int left, const int right)
{
    /***** Complete this member function. *****/
	if(left >= right) return;

    int pivot = choose_pivot(left, right);
    int partionIndex = partition(left, right, pivot);
    quicksort(left, partionIndex-1);
    quicksort(partionIndex+1, right);
}

/**
 * Choose the pivot according to a pivot strategy.
 * The chosen pivot will be moved temporarily to the right end.
 * @param left the left index of the partition to sort.
 * @param right the right index of the partition to sort.
 * @return the pivot value.
 */
int QuickSorter::choose_pivot(const int left, const int right)
{
    return choose_pivot_strategy(left, right);
}

/**
 * Partition the subrange by moving data elements < pivot to the left
 * and data elements > pivot to the right.
 * @param left the left index of the partition to sort.
 * @param right the right index of the partition to sort.
 * @param pivot the pivot value.
 */
int QuickSorter::partition(const int left, const int right, const int pivot)
{
    /***** Complete this member function. *****/
    int pivotIdx = 0;

    for(int s = left; s <= right; s++)
    {
    	if(data[s] == pivot)
    	{
    		pivotIdx = s;
    		break;
    	}
    }

    // move the pivot to the very right index
    swap(right, pivotIdx);

    int pIndex = left;
    for(int i = left; i < right; i++)
    {
    	if(data[i] < pivot)
    	{
    		compare_count++;
    		swap(i,pIndex);
    		pIndex++;
    	}
    	else
    	{
    		compare_count++;
    	}
    }
    if(data[right] > data[pIndex])
    {
    	compare_count++;
    	return right;
    }
    else
    {
    	compare_count++;
    	swap(pIndex, right);
	    return pIndex;
    }
}
