/*
 * Assignment #12: Compare sorting algorithms
 *
 * CMPE 180-92 Data Structures and Algorithms in C++
 * Department of Computer Engineering
 * R. Mak, Nov. 20, 2016
 */
#include <string>
#include "MergeSort.h"

/**
 * Default constructor.
 */
MergeSort::MergeSort() : ListSorter() {}

/**
 * Destructor.
 */
MergeSort::~MergeSort() {}

/**
 * Get the name of this sorting algorithm.
 * @return the name.
 */
string MergeSort::name() const { return "Mergesort"; }

/**
 * Run the mergesort algorithm.
 * @throws an exception if an error occurred.
 */
void MergeSort::run_sort_algorithm()
    throw (string)
{
    // Defensive programming: Make sure we end up
    // with the same size list after sorting.
    int size_before = data.get_size();

    mergesort(data);

    int size_after = data.get_size();
    if (size_before != size_after)
    {
        string message = "***** Size mismatch: before " +
                         to_string(size_before) + ", size after " +
                         to_string(size_after);
        throw message;
    }
}

/**
 * The mergesort algorithm recursively splits and merges data lists.
 * @param list the list of data to sort.
 */
void MergeSort::mergesort(LinkedList& list)
{
    /***** Complete this member function. *****/
    if(list.get_size() < 1 || list.get_head() == nullptr)
    {
        return;
    }
    else
    {
        LinkedList list1;// = new LinkedList();
        LinkedList list2;// = new LinkedList();
        list.split(list1, list2);
        list1.print();
        list2.print();
        merge(list, list1, list2);
        list.print();
    }
}

/**
 * Merge two sublists back into a single list.
 * @param list the merged list.
 * @param sublist1 the first sublist.
 * @param sublist2 the second sublist.
 */
void MergeSort::merge(LinkedList& list,
                      LinkedList& sublist1, LinkedList& sublist2)
{
    /***** Complete this member function. *****/
    int i = 0;
    int j = 0;
    Node *node1 = sublist1.get_head();
    Node *node2 = sublist2.get_head();

    Node *listNode = list.get_head();

    bool sub1Done = false;
    bool sub2Done = false;

    while(true)
    {
        // compare elements from each sublist
        int val1 = node1->value;
        int val2 = node2->value;

        if(val1 > val2)
        {
            // element on the left list (sublist1) is larger
            // put val2 on the result list
            //list.add(val2);
            //listNode->value = val2;
            listNode = node2;
            listNode = listNode->next;
            node2 = node2->next;
            j++;
        }
        else
        {
            //list.add(val1);
            //listNode->value = val1;
            listNode = node1;
            listNode = listNode->next;
            node1 = node1->next;
            i++;
        }

        // check to see if any of the lists have reached at the end
        if(i == sublist1.get_size())
        {
            // sublist1 has reached the end, leave the whiel loop
            sub1Done = true;
            break;
        }
        if(j == sublist2.get_size())
        {
            // sublist2 has reached the end, leave the while loop
            sub2Done = true;
            break;
        }
    }

    if(sub1Done)
    {  
        // sub1 reached the end, complete the sublist2
        for(int idx = j; idx < sublist2.get_size(); idx++)
        {
            //int val2 = node2->value;
            //list.add(val2);
            //listNode->value = val2;
            listNode = node2;
            listNode = listNode->next;
            node2 = node2->next;
        }
    }

    if(sub2Done)
    {
        // sub2 reached the end, complete the sublist1
        for(int idx = i; idx < sublist1.get_size(); idx++)
        {
            //int val1 = node1->value;
            //list.add(val1);
            //listNode->value = val1;
            listNode = node1;
            listNode = listNode->next;
            node1 = node1->next;
        }
    }
}

/**
 * Clear the data.
 */
void MergeSort::clear() { data.clear(); }
