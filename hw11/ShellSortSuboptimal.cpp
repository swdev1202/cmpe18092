/*
 * Assignment #12: Compare sorting algorithms
 *
 * CMPE 180-92 Data Structures and Algorithms in C++
 * Department of Computer Engineering
 * R. Mak, Nov. 20, 2016
 */
#include "ShellSortSuboptimal.h"

/**
 * Default constructor.
 */
ShellSortSuboptimal::ShellSortSuboptimal() {}

/**
 * Destructor.
 */
ShellSortSuboptimal::~ShellSortSuboptimal() {}

/**
 * Get the name of this sorting algorithm.
 * @return the name.
 */
string ShellSortSuboptimal::name() const { return "Shellsort suboptimal"; }

/**
 * Run the suboptimal shellsort algorithm.
 * @throws an exception if an error occurred.
 */
void ShellSortSuboptimal::run_sort_algorithm() throw (string)
{
	/*int h = size/2; // start with the half the size of the vector

	while(h > 1)
	{
		int i = 0;
		while((i+h) < size)
		{
			compare_count++;
			if(data[i+h] < data[i])
			{
				// if the h-apart element from i is less than i
				swap(i, i+h);
			}
			i++;
		}
		h = h/2;
	}*/

	/*
	 * Source Code Reference
	 * GeeksQuiz
	 * http://quiz.geeksforgeeks.org/shellsort/
	 */
	 
	for(int h = size/2; h>0; h /= 2)
	{
		for(int i=h; i<size; i++)
		{
			int currElement = data[i];
			int j;
			compare_count++;
			for(j = i; j >= h && data[j-h] > currElement; j -= h)
			{
				move_count++;
				data[j] = data[j-h];
			}
			if(j == i)
			{
				// not moved
			}
			else
			{
				move_count++;
				data[j] = currElement;
			}
		}
	}

}
