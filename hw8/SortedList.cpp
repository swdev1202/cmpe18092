#include <iostream>
#include <iterator>
#include "SortedList.h"

using namespace std;

SortedList::SortedList()
{
}

SortedList::~SortedList()
{
}

void SortedList::prepend(int value)
{
	list<int>::iterator it = data.begin();
	it = data.insert(it, value);
}

void SortedList::append(int value)
{
	data.push_back(value);
}

bool SortedList::find(int value) const
{
	list<int>::const_iterator it = data.begin();
	for(;it != data.end(); it++)
	{
		if(*it == value)
			return true;
	}
	/*while(it != data.end())
	{
		if(*it == value)
		{
			return true;
		}
		it++;
	}*/
	return false;
}

int SortedList::get_value(int i) const
{
	list<int>::const_iterator it;
	if(i < (data.size()/2)) // if the lower half
	{
		int cnt = i;
		for(it = data.begin(); it != data.end(); it++)
		{
			if(cnt == 0)
			{
				// reached the end
				return *it;
			}
			else
			{
				cnt--;
			}
		}
	}
	else // if the upper half
	{
		int cnt = data.size()-1;
		//for(list<int>::reverse_iterator rit = data.rbegin(); rit != data.rend(); rit++)
		for(reverse_iterator<list<int>::const_iterator> rit = data.rbegin(); rit != data.rend(); rit++)
		{
			if(cnt == i)
			{
				return *rit;
			}
			else
			{
				cnt--;
			}
		}
	}
}

void SortedList::clear()
{
	list<int>::iterator it = data.begin();
	while(it != data.end())
	{
		it = data.erase(it);
		it = data.begin();
	}
}

bool SortedList::check()
{
    if (data.size() == 0) return true;

    list<int>::iterator it = data.begin();
    int prev = *it;
    while ((++it != data.end()) && (prev <= *it));
    return it == data.end();
}
