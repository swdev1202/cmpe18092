#include <iostream>
#include <iterator>
#include "SortedVector.h"

using namespace std;

SortedVector::SortedVector()
{
}

SortedVector::~SortedVector()
{
}

void SortedVector::prepend(int value)
{
	vector<int>::iterator it = data.begin();
	it = data.insert(it, value);
}

void SortedVector::append(int value)
{
	data.push_back(value);
}

bool SortedVector::find(int value) const
{
	vector<int>::const_iterator it = data.begin();
	for(;it != data.end(); it++)
	{
		if(*it == value)
			return true;
	}
	/*
	while(it != data.end())
	{
		if(*it == value)
		{
			//cout << "Value : " << value << "*it : " << *it << endl;
			return true;
		}
		it++;
	}*/
	return false;
}

int SortedVector::get_value(int i) const
{
	return data.at(i);
}

void SortedVector::clear()
{
	vector<int>::iterator it = data.begin();
	while(it != data.end())
	{
		it = data.erase(it);
	}
}

bool SortedVector::check()
{
    if (data.size() == 0) return true;

    vector<int>::iterator it = data.begin();
    int prev = *it;
    while ((++it != data.end()) && (prev <= *it));
    return it == data.end();
}
