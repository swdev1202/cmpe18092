#include <iostream>
#include <iomanip>
#include "OpenAddressHT.h"
#include "HashEntry.h"

using namespace std;

/**
 * Default constructor
 */
OpenAddressHT::OpenAddressHT() : HashTable() {}

/**
 * Constructor. Initialize each table slot to nullptr.
 */
OpenAddressHT::OpenAddressHT(int size) : HashTable(size)
{
    // completed
    for(int i=0; i<size; i++)
    {
        table.push_back(nullptr);
    }
}

/**
 * Destructor. Delete all the entries in the table.
 */
OpenAddressHT::~OpenAddressHT()
{
    for(int i = 0; i<get_size(); i++)
    {
        table.pop_back();
    }
}

/**
 * Return an entry at a given index of the hash table.
 * @param index the index.
 * @return the entry.
 */
HashEntry *OpenAddressHT::get_entry(const int index) const
{
    /***** Complete this member function. *****/
    return table.at(index);
}

/**
 * Search for a given table entry. Count the number of probes.
 * @param key the key of the entry to find.
 * @param probe_count the cumulative count of probes.
 * @return the hash table index of the entry if found, else of an empty slot.
 */
int OpenAddressHT::search(const string& key, int& probe_count)
{
    // Initial probe index.
    int index = hash(key);
    probe_count++;

    if(table.at(index) == nullptr)
    {
        // if the given index is empty,
        index = index;
    }
    else
    {
        if(table.at(index)->word == key)
        {
            // if the given index has the same key,
            index = index;
        }
        else
        {
            // if the given index has a different key,
            // start looking for the key
            while(true)
            {
                index = next_index(index);
                probe_count++;
                if(table.at(index) != nullptr)
                {
                    if(table.at(index)->word == key)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }
    return index;
}

/**
 * If an entry isn't already in the table, enter it with a
 * frequency count of 1. If the entry already exists,
 * update it by incrementing its frequency count by 1.
 * Count the number of probes from the initial search.
 * @param key the key of the entry to enter or update.
 * @return the index of the newly entered or updated entry.
 */
int OpenAddressHT::enter(const string& key, int& probe_count)
{
    // Search for the entry key.
    int index = search(key, probe_count);

    if(table.at(index) == nullptr)
    {
        // if it's first time entering the content,
        HashEntry *newHashEntry = new HashEntry(key);
        table.at(index) = newHashEntry;
    }
    else
    {
        table.at(index)->count++;
    }

    return index;
}

/**
 * Print the contents of the hash table.
 * Skip empty table slots.
 */
void OpenAddressHT::dump() const
{
    /***** Complete this member function. *****/
    
    int cnt = 0;
    for(int i=0; i<get_size(); i++)
    {
        if(table.at(i) != nullptr)
        {
            cout << cnt << ":";
            cout << setw(4) << table.at(i)->count << "-" << table.at(i)->word << endl;
            //cnt++;
        }
        cnt++;
    }
}
