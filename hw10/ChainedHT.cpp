#include <iostream>
#include <iomanip>
#include "ChainedHT.h"
using namespace std;

/**
 * Default constructor.
 */
ChainedHT::ChainedHT() : HashTable() {}

/**
 * Constructor. Initialize each table slot to nullptr.
 */
ChainedHT::ChainedHT(int size) : HashTable(size)
{
    /***** Complete this member function. *****/
    for(int i = 0; i < size; i++)
    {
        table.push_back(nullptr);
    }
}

/**
 * Destructor. Delete all the entries in the collision chains.
 */
ChainedHT::~ChainedHT()
{
    /***** Complete this member function. *****/
    for(int i=0; i<get_size(); i++)
    {
        table.pop_back();
    }
}

/**
 * Search for a given table entry. Count the number of probes.
 * @param key the key of the entry to find.
 * @param probe_count the cumulative count of probes.
 * @return a pointer to the entry if found, else return nullptr.
 */
HashEntry *ChainedHT::search(const string& word, int& probe_count)
{
    int index = hash(word);

    HashEntry *retEntry = nullptr;

    if(table.at(index) == nullptr)
    {
        // if there were nothing
        retEntry = nullptr;
    }
    else
    {
        HashNode* temp = table.at(index);
        probe_count++;
        if(temp->entry->word == word)
        {
            // if found at the first node
            retEntry = temp->entry;
        }
        else
        {
            while(true)
            {
                if(temp->next != nullptr)
                {
                    probe_count++;
                    temp = temp->next; // if the next is not empty
                }
                else
                {
                    // if the next is null point, this node is the last
                    if(temp->entry->word == word)
                    {
                        retEntry = temp->entry;
                        break;
                    }
                    else
                    {
                        retEntry = nullptr;
                        break;
                    }
                }

                if(temp->entry->word == word)
                {
                    retEntry = temp->entry;
                    break;
                }
            }
        }
    }

    return retEntry;
}

/**
 * If an entry isn't already in the table, enter it with a
 * frequency count of 1. If the entry already exists,
 * update it by incrementing its frequency count by 1.
 * Count the number of probes from the initial search.
 * @param key the key of the entry to enter or update.
 * @return a pointer to the newly entered or updated entry.
 */
HashEntry *ChainedHT::enter(const string& word, int& probe_count)
{
    HashEntry *entry_ptr = search(word, probe_count);
    int index = hash(word);

    if(entry_ptr == nullptr)
    {
        // couldn't find it, so make a new
        entry_ptr = new HashEntry(word); // word & count= 1
        HashNode *insrtHashNode = new HashNode(entry_ptr);
        if(table.at(index) == nullptr)
        {
            // if the table is empty
            table.at(index) = insrtHashNode;
        }
        else
        {
            // if the table index location already had a node
            HashNode* temp = table.at(index);
            insrtHashNode->next = temp;
            table.at(index) = insrtHashNode;
        }
    }
    else
    {
        //found it
        HashNode* temp = table.at(index);
        if(temp->entry->word == entry_ptr->word)
        {
            entry_ptr->count++;
        }
        else
        {
            while(temp->next != nullptr)
            {
                temp = temp->next;
                if(temp->entry->word == entry_ptr->word)
                {
                    entry_ptr->count++;
                }
            }
        }
    }

    return entry_ptr;
}

/**
 * Print the contents of the hash table.
 * Skip empty table slots.
 */
void ChainedHT::dump() const
{
    /***** Complete this member function. *****/
    for(int i=0; i<get_size(); i++)
    {
        if(table.at(i) == nullptr)
        {
            //if the table location is empty
            //do not print
        }
        else
        {
            HashNode* temp = table.at(i);
            cout << setw(4) << i << ":" << endl;
            cout << setw(10) << temp->entry->count << "-";
            cout << temp->entry->word << endl;
            while(temp->next != nullptr)
            {
                temp = temp->next;
                cout << setw(10) << temp->entry->count << "-";
                cout << temp->entry->word << endl;
            }
        }
    }

}

/**
 * Compute the average chain length.
 */
double ChainedHT::avg_chain_length() const
{
    /***** Complete this member function. *****/
    // compute for the total chain lengths
    int totalChainCnt = 0;
    for(int i=0; i<get_size(); i++)
    {
        if(table.at(i) == nullptr)
        {
            totalChainCnt += 0;
        }
        else
        {
            HashNode* temp = table.at(i);
            totalChainCnt++;
            if(temp->next != nullptr)
            {
                // if the next node is not nullptr
                while(true)
                {
                    temp = temp->next;
                    totalChainCnt++;
                    if(temp->next == nullptr)
                    {
                        // it was the last node
                        break;
                    }
                }
            }
        }
    }
    return double(totalChainCnt) / get_size();
}

